﻿using FPS.BusinessLayer.ViewModel.User;

namespace FPS.MVC.Service.Account
{
    public interface IAccountService
    {
        public Task<AccountLogin> Login(string email, string password);
        public Task<bool> forgotPassword(string email);
    }
}
