﻿
using FPS.BusinessLayer.ViewModel.Company;
using FPS.BusinessLayer.ViewModel.User;
using FPS.MVC.ApiConfig;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using NuGet.Common;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;

namespace FPS.MVC.Service.Account
{
    public class AccountService : BaseService, IAccountService
    {
        public AccountService(HttpClient client, IHttpContextAccessor httpContextAccessor) : base(client, httpContextAccessor)
        {
        }

        // Hàm tạo mật khẩu ngẫu nhiên với độ dài xác định
        public static string GeneratePassword(int length)
        {
            const string allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            byte[] randomBytes = new byte[length];

            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomBytes);
            }

            char[] chars = new char[length];
            int allowedCharCount = allowedChars.Length;

            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[randomBytes[i] % allowedCharCount];
            }

            return new string(chars);
        }

        public async Task<bool> forgotPassword(string email)
        {
            var password = "";
            var loginData = new { email, password };
            var json = JsonConvert.SerializeObject(loginData);
            var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(PathConfig.POST_FORGOTPASSWORD, content);

            if (response.IsSuccessStatusCode)
            {
                var responseAcounnt = await _client.GetAsync(PathConfig.GET_ACCOUNT_EMAIL + email);
                var IdAccountJson = await responseAcounnt.Content.ReadAsStringAsync();
                var IdAccount = JsonConvert.DeserializeObject<IdentityUser>(IdAccountJson);

                var passwordHash = GeneratePassword(12);
                var emailToId = email;
                var emailToName = "canviet";
                var emailSubject = "Mail forgot Password";
                var emailBody = passwordHash;
                IdentityUser user = new IdentityUser();
                var AccountChangePass = new
                {
                    IdAccount.Id,
                    user.UserName,
                    user.NormalizedUserName,
                    user.Email,
                    user.NormalizedEmail,
                    user.EmailConfirmed
                    ,
                    passwordHash,
                    user.SecurityStamp,
                    user.ConcurrencyStamp,
                    user.PhoneNumber,
                    user.PhoneNumberConfirmed,
                    user.TwoFactorEnabled,
                    user.LockoutEnd
                ,
                    user.LockoutEnabled,
                    user.AccessFailedCount
                };
                var jsonChangePass = JsonConvert.SerializeObject(AccountChangePass);
                var ChangePass = new StringContent(jsonChangePass, System.Text.Encoding.UTF8, "application/json");
                await _client.PutAsync(PathConfig.PUT_FORGOTPASSWORD, ChangePass);

                var mail = new { emailToId, emailToName, emailSubject, emailBody };
                var jsonMail = JsonConvert.SerializeObject(mail);
                var mailcontent = new StringContent(jsonMail, System.Text.Encoding.UTF8, "application/json");

                var mailreponse = await _client.PostAsync(PathConfig.SENDMAIL, mailcontent);
                if (mailreponse.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public async Task<AccountLogin> Login(string email, string password)
        {
            var loginData = new { email, password };
            var json = JsonConvert.SerializeObject(loginData);
            var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(PathConfig.GET_LOGIN, content);

            if (response.IsSuccessStatusCode)
            {
                var tokenResponse = await response.Content.ReadAsStringAsync();
                var token = JsonConvert.DeserializeObject<TokenResponse>(tokenResponse);
                return new AccountLogin
                {
                    IsLoggedIn = true,
                    Token = token.Token.ToString()
                };
            }
            else
            {
                return new AccountLogin
                {
                    IsLoggedIn = false,
                    Token = null
                };
            }
        }
    }
}
