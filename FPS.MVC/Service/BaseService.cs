﻿using System.Net.Http.Headers;

namespace FPS.MVC.Service
{
    public class BaseService
    {
        public readonly HttpClient _client;
        public readonly IHttpContextAccessor _httpContextAccessor;
        public BaseService(HttpClient client, IHttpContextAccessor httpContextAccessor)
        {
            _client = client;
            _httpContextAccessor = httpContextAccessor;
        }
    }
}
