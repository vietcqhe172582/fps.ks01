﻿using FPS.BusinessLayer.ViewModel.MonthPlanning;

namespace FPS.MVC.Service.MonthPlanning
{
    public interface IMonthPlanningService
    {
        public Task<List<MonthPlanningView>> GetAllMonthPlanning();
        public Task<string> AddMonthPlanning(MonthPlanningModel model);
        public Task UpdateMonthPlanning(MonthPlanningModel model);
        public Task DeleteMonthPlanning(Guid id);
    }
}
