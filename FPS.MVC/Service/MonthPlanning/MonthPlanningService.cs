﻿
using FPS.BusinessLayer.ViewModel.Company;
using FPS.BusinessLayer.ViewModel.MonthPlanning;
using FPS.MVC.ApiConfig;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace FPS.MVC.Service.MonthPlanning
{
    public class MonthPlanningService : BaseService, IMonthPlanningService
    {
        public MonthPlanningService(HttpClient client, IHttpContextAccessor httpContextAccessor) : base(client, httpContextAccessor)
        {
            var accessToken = httpContextAccessor.HttpContext.Session.GetString("AccessToken");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
        }

        public async Task<string> AddMonthPlanning(MonthPlanningModel model)
        {
            var NewMonthPlanning = new {model.TermName, model.StartedDate, model.EndDate, model.Duration, model.PlanDueDate, model.ReportDueDate, model.AccountantID};
            var json = JsonConvert.SerializeObject(NewMonthPlanning);
            var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(PathConfig.POST_ADD_MONTHPLANNING, content);
            if (response.IsSuccessStatusCode)
            {
                return "Add New Successfull";
            }
            else
            {
                return "Falid to Add";
            }
        }

        public Task DeleteMonthPlanning(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<MonthPlanningView>> GetAllMonthPlanning()
        {
            if (_httpContextAccessor.HttpContext.Session.GetString("AccountLogin") == null)
            {
                return null;
            }
            var response = await _client.GetAsync(PathConfig.GET_ALL_MONTHPLANNING);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }
            else
            {
                string apiResponse = await response.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<List<MonthPlanningView>>(apiResponse);

                return data;
            }
            
        }

        public Task UpdateMonthPlanning(MonthPlanningModel model)
        {
            throw new NotImplementedException();
        }
    }
}
