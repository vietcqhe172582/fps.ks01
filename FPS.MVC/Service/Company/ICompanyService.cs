﻿using FPS.BusinessLayer.ViewModel.Company;

namespace FPS.MVC.Service.Company
{
    public interface ICompanyService
    {
        public Task<List<CompanyViewModel>> GetAllCompany();
    }
}
