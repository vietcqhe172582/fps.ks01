﻿
using FPS.BusinessLayer.ViewModel.Company;
using FPS.MVC.ApiConfig;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;

namespace FPS.MVC.Service.Company
{
    public class CompanyService :  BaseService, ICompanyService
    {
        public CompanyService(HttpClient client, IHttpContextAccessor httpContextAccessor) : base(client, httpContextAccessor)
        {
            var accessToken = httpContextAccessor.HttpContext.Session.GetString("AccessToken");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
        }

        public async Task<List<CompanyViewModel>> GetAllCompany()
        {
            if (_httpContextAccessor.HttpContext.Session.GetString("AccountLogin") == null)
            {
                return null;
            }
            var response = await _client.GetAsync(PathConfig.GET_ALL_COMPANY);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            string apiResponse = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<List<CompanyViewModel>>(apiResponse);

            return data;
        }
    }
}
