﻿using FPS.BusinessLayer.ViewModel.DetailPlannning;

namespace FPS.MVC.Service.DepartmentPlanningDetail
{
    public interface IDepartmentPlanningDetailService
    {
        public Task<List<DetailPlanningExecel>> GetExcel(string Id);
    }
}
