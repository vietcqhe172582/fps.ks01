﻿
using FPS.BusinessLayer.ViewModel.DetailPlannning;
using FPS.MVC.ApiConfig;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace FPS.MVC.Service.DepartmentPlanningDetail
{
    public class DepartmentPlanningDetailService : BaseService, IDepartmentPlanningDetailService
    {
        public DepartmentPlanningDetailService(HttpClient client, IHttpContextAccessor httpContextAccessor) : base(client, httpContextAccessor)
        {
            var accessToken = httpContextAccessor.HttpContext.Session.GetString("AccessToken");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
        }

        public async Task<List<DetailPlanningExecel>> GetExcel(string Id)
        {
            if (_httpContextAccessor.HttpContext.Session.GetString("AccountLogin") == null)
            {
                return null;
            }
            var response = await _client.GetAsync(PathConfig.GET_EXCEL + Id);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            string apiResponse = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<List<DetailPlanningExecel>>(apiResponse);

            return data;
        }
    }
}
