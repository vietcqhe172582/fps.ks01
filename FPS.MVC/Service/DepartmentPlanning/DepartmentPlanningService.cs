﻿
using FPS.BusinessLayer.ViewModel.Company;
using FPS.BusinessLayer.ViewModel.DepartmentPlanning;
using FPS.MVC.ApiConfig;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace FPS.MVC.Service.DepartmentPlanning
{
    public class DepartmentPlanningService : BaseService, IDepartmentPlanningService
    {
        public DepartmentPlanningService(HttpClient client, IHttpContextAccessor httpContextAccessor) : base(client, httpContextAccessor)
        {
            var accessToken = httpContextAccessor.HttpContext.Session.GetString("AccessToken");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
        }

        public async Task<List<DepartmentPlanningView>> GetAllDepartmentPlanning()
        {
            if (_httpContextAccessor.HttpContext.Session.GetString("AccountLogin") == null)
            {
                return null;
            }
            var response = await _client.GetAsync(PathConfig.GET_ALL_DEPARTMENTPLANNING);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            string apiResponse = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<List<DepartmentPlanningView>>(apiResponse);

            return data;
        }

        public async Task<List<DepartmentPlanningView>> GetAllDepartmentPlanningByTerm(Guid Id)
        {
            if (_httpContextAccessor.HttpContext.Session.GetString("AccountLogin") == null)
            {
                return null;
            }
            string ID = Id.ToString();
            var response = await _client.GetAsync(PathConfig.GET_TERM_DEPARTMENTPLANNING + ID);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            string apiResponse = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<List<DepartmentPlanningView>>(apiResponse);

            return data;
        }
    }
}
