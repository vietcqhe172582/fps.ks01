﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.BusinessLayer.ViewModel.DepartmentPlanning;

namespace FPS.MVC.Service.DepartmentPlanning
{
    public interface IDepartmentPlanningService
    {
        public Task<List<DepartmentPlanningView>> GetAllDepartmentPlanning();
        public Task<List<DepartmentPlanningView>> GetAllDepartmentPlanningByTerm(Guid Id);
    }
}
