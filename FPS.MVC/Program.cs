using FPS.BusinessLayer.Repository.Company;
using FPS.MVC.Service.Account;
using FPS.MVC.Service.Company;
using FPS.MVC.Service.DepartmentPlanning;
using FPS.MVC.Service.DepartmentPlanningDetail;
using FPS.MVC.Service.MonthPlanning;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddHttpClient<ICompanyService, CompanyService>(x => x.BaseAddress = new Uri("https://localhost:7103/"));
builder.Services.AddHttpClient<IAccountService, AccountService>(x => x.BaseAddress = new Uri("https://localhost:7103/"));
builder.Services.AddHttpClient<IMonthPlanningService, MonthPlanningService>(x => x.BaseAddress = new Uri("https://localhost:7103/"));
builder.Services.AddHttpClient<IDepartmentPlanningService, DepartmentPlanningService>(x => x.BaseAddress = new Uri("https://localhost:7103/"));
builder.Services.AddHttpClient<IDepartmentPlanningDetailService, DepartmentPlanningDetailService>(x => x.BaseAddress = new Uri("https://localhost:7103/"));
builder.Services.AddHttpContextAccessor();

builder.Services.AddDistributedMemoryCache();
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(10);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
	app.UseExceptionHandler("/Home/Error");
	// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
	app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseSession();

app.UseAuthorization();

app.MapControllerRoute(
	name: "default",
	pattern: "{controller=Account}/{action=Login}/{id?}");

app.Run();
