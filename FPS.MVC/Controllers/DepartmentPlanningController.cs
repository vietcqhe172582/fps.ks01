﻿using FPS.MVC.Service.DepartmentPlanning;
using Microsoft.AspNetCore.Mvc;

namespace FPS.MVC.Controllers
{
    public class DepartmentPlanningController : Controller
    {
        private readonly IDepartmentPlanningService _departmentPlanningService;
        public DepartmentPlanningController(IDepartmentPlanningService departmentPlanningService)
        {
            _departmentPlanningService = departmentPlanningService;
        }

        public async Task<IActionResult> Index()
        {
            var departmentPlannings = await _departmentPlanningService.GetAllDepartmentPlanning();
            if (departmentPlannings == null)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(departmentPlannings);
        }
        public async Task<IActionResult> ListPlanningTerm(Guid id)
        {
            var departmentPlannings = await _departmentPlanningService.GetAllDepartmentPlanningByTerm(id);
            if (departmentPlannings == null)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(departmentPlannings);
        }
    }
}
