﻿using FPS.BusinessLayer.Repository.Company;
using FPS.MVC.Service.Company;
using Microsoft.AspNetCore.Mvc;

namespace FPS.MVC.Controllers
{
    public class CompanyController : Controller
    {
        private readonly ICompanyService _companyService;
        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }
        public async Task<IActionResult> Index()
        {
            var companies = await _companyService.GetAllCompany();
            if (companies == null)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(companies);
        }
    }
}
