﻿using FPS.MVC.Service.MonthPlanning;
using Microsoft.AspNetCore.Mvc;

namespace FPS.MVC.Controllers
{
    public class MonthPlanningController : Controller
    {
        private readonly IMonthPlanningService _monthPlanningService;
        public MonthPlanningController(IMonthPlanningService monthPlanningService)
        {
            _monthPlanningService = monthPlanningService;
        }

        public async Task<IActionResult> Index()
        {
            var monthPlannings = await _monthPlanningService.GetAllMonthPlanning();
            if (monthPlannings == null)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(monthPlannings);
        }
    }
}
