﻿using FPS.MVC.Service.Account;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NuGet.Common;

namespace FPS.MVC.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        public async Task<IActionResult> Login()
        {
            return View();
        }

        public async Task<IActionResult> forgotPassword()
        {
            return View();
        }
        public async Task<IActionResult> Logout()
        {
            // Xóa mọi thông tin phiên của người dùng
            HttpContext.Session.Clear();
            // Trả về trang đăng nhập
            return RedirectToAction("Login", "Account");

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> forgotPassword(string email)
        {

            bool loginResult = await _accountService.forgotPassword(email);
            if (loginResult)
            {
                // Đăng nhập thành công, chuyển hướng đến trang chính của ứng dụng
                return RedirectToAction("Index", "Home");
            }
            else
            {
                // Đăng nhập không thành công, hiển thị thông báo lỗi
                ModelState.AddModelError(string.Empty, "Tên đăng nhập hoặc mật khẩu không đúng.");
                return View(); // hoặc return RedirectToAction("Login", "Account");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(string email, string password)
        {

            var loginResult = await _accountService.Login(email, password);

            if (loginResult.IsLoggedIn)
            {
                // Đăng nhập thành công, chuyển hướng đến trang chính của ứng dụng
                HttpContext.Session.SetString("AccessToken",loginResult.Token);
                HttpContext.Session.SetString("AccountLogin", email);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                // Đăng nhập không thành công, hiển thị thông báo lỗi
                ModelState.AddModelError(string.Empty, "Tên đăng nhập hoặc mật khẩu không đúng.");
                return View(); // hoặc return RedirectToAction("Login", "Account");
            }
        }

       
    }
}
