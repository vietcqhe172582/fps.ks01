﻿using FPS.MVC.Service.DepartmentPlanningDetail;
using Microsoft.AspNetCore.Mvc;

namespace FPS.MVC.Controllers
{
    public class DepartmentPlanningDetailController : Controller
    {
        private readonly IDepartmentPlanningDetailService _departmentPlanningDetailService;
        public DepartmentPlanningDetailController(IDepartmentPlanningDetailService departmentPlanningDetailService)
        {
            _departmentPlanningDetailService = departmentPlanningDetailService;
        }

        public async Task<IActionResult> Index(string id)
        {
            var Excel = await _departmentPlanningDetailService.GetExcel(id);
            if (Excel == null)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(Excel);
        }
    }
}
