﻿namespace FPS.MVC.ApiConfig
{
    public class PathConfig
    {
        //Company
        public const string GET_ALL_COMPANY = "/api/Company/GetAll";
        
        //Month Planning
        public const string GET_ALL_MONTHPLANNING = "api/MonthPlanning/GetAll";
        public const string POST_ADD_MONTHPLANNING = "api/MonthPlanning/AddMonthPlanning";
        public const string PUT_STARTTERM = "api/MonthPlanning/StartMonthPlanning";
        public const string PUT_UPDATE_MONTHPLANNING = "api/MonthPlanning/UpdateMonthPlanning";
        public const string DELETE_DELETE_MONTHPLANNING = "api/MonthPlanning/DeleteMonthPlanning";

        //Department Plannning
        public const string GET_ALL_DEPARTMENTPLANNING = "api/DepartmentPlanning/GetAllDepartmentPlanning";
        public const string GET_ID_DEPARTMENTPLANNING = "api/DepartmentPlanning/GetDepartmentPlanningByID";
        public const string GET_TERM_DEPARTMENTPLANNING = "api/DepartmentPlanning/GetAllDepartmentPlanningByTerm?TermID=";

        //Department Planning Detail
        public const string GET_EXCEL = "api/DepartmentPlanningDetail/GetExcel?Id=";


        //Account
        public const string GET_LOGIN = "api/Account/Login";
        public const string POST_FORGOTPASSWORD = "api/Account/ForgotPassword";
        public const string SENDMAIL = "api/Mail/SendMail";
        public const string PUT_FORGOTPASSWORD = "api/Account/UpdateUser";
        public const string GET_ACCOUNT_ID = "api/Account/GetAccount";
        public const string GET_ACCOUNT_EMAIL = "api/Account/GetAccountByEmail?email=";
    }
}
