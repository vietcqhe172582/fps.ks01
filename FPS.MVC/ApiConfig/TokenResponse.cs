﻿namespace FPS.MVC.ApiConfig
{
    public class TokenResponse
    {
        public string Token { get; set; }
    }
}
