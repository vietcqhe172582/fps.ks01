﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.BusinessLayer.ViewModel.DepartmentPlanning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.DepartmentPlannning
{
    public interface IDepartmentPlanningRepository
    {
        public void AddDepartmentPlanning(DepartmentPlanningModel model);
        public void UpdateDepartmentPlanning(DepartmentPlanningModel model);
        public void DeleteDepartmentPlanning(Guid Id);
        public IEnumerable<DepartmentPlanningView> GetAll();
        public IEnumerable<DepartmentPlanningView> GetPlanningByTermID(Guid TermID);
        public FPS.DataLayer.Entity.DepartmentPlanning GetDepartmentPlanning(Guid id);
    }
}
