﻿using FPS.BusinessLayer.ViewModel.DepartmentPlanning;
using FPS.DataLayer.Context;
using FPS.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.DepartmentPlannning
{
    public class DepartmentPlanningRepository : BaseRepository, IDepartmentPlanningRepository
    {
        public DepartmentPlanningRepository(FPSContext context) : base(context)
        {
        }

        public void AddDepartmentPlanning(ViewModel.DepartmentPlanning.DepartmentPlanningModel model)
        {
            FPS.DataLayer.Entity.DepartmentPlanning departmentPlanning = new FPS.DataLayer.Entity.DepartmentPlanning();
            departmentPlanning.DepartmentPlanningName = model.DepartmentPlanningName;
            departmentPlanning.FinancialStaffID = Guid.Parse(model.FinncialStaffID);
            departmentPlanning.TermID = Guid.Parse(model.TermID);
            departmentPlanning.CreateBy = Guid.NewGuid();
            departmentPlanning.UpdatedBy = Guid.NewGuid();
            departmentPlanning.CreateDate = DateTime.Now;

            _context.DepartmentPlannings.Add(departmentPlanning);
            _context.SaveChanges();
        }

        public void DeleteDepartmentPlanning(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DepartmentPlanningView> GetAll()
        {
            var departmentPlannings = _context.DepartmentPlannings.ToList();
            var listDepartmentPlanning = departmentPlannings.Select(x => new DepartmentPlanningView
            {
                ID = x.Id.ToString(),
                DepartmentPlanningName = x.DepartmentPlanningName,
                CreateDate = x.CreateDate
            });
            return listDepartmentPlanning;
        }

        public DataLayer.Entity.DepartmentPlanning GetDepartmentPlanning(Guid id)
        {
            var departmentPlanning = _context.DepartmentPlannings.FirstOrDefault(x => x.Id == id);

            return departmentPlanning;
        }

        public IEnumerable<DepartmentPlanningView> GetPlanningByTermID(Guid TermID)
        {
            var departmentPlannings = _context.DepartmentPlannings.Where(x => x.TermID == TermID).ToList();
            var listDepartmentPlanning = departmentPlannings.Select(x => new DepartmentPlanningView
            {
                ID = x.Id.ToString(),
                DepartmentPlanningName = x.DepartmentPlanningName,
                CreateDate = x.CreateDate
            });
            return listDepartmentPlanning;
        }

        public void UpdateDepartmentPlanning(ViewModel.DepartmentPlanning.DepartmentPlanningModel model)
        {
            throw new NotImplementedException();
        }
    }
}
