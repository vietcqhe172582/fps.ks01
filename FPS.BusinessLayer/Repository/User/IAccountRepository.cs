﻿using FPS.BusinessLayer.ViewModel.User;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.User
{
    public interface IAccountRepository
    {
        public IdentityUser GetAccount(string Id);
        public IdentityUser GetAccountByEmail(string Email);
        public IEnumerable<IdentityUser> GetAll();
        public void Add(IdentityUser user);
        public void AddRole(IdentityUserRole<string> user);
        public void Update(IdentityUser user);
        public void Delete(IdentityUser user);
        public bool Login(UserModel user);
        public bool forgotPassword(UserModel user);
        public string GenerateToken(UserModel user);
    }
}
