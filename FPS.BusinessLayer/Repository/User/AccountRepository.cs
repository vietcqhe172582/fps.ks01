﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.BusinessLayer.ViewModel.User;
using FPS.DataLayer.Context;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace FPS.BusinessLayer.Repository.User
{
    public class AccountRepository : BaseRepository, IAccountRepository
    {
        private readonly IConfiguration _config;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IPasswordHasher<IdentityUser> _passwordHasher;

        public AccountRepository(IConfiguration config, UserManager<IdentityUser> userManager, IPasswordHasher<IdentityUser> passwordHasher, FPSContext context) : base(context)
        {
            _config = config;
            _userManager = userManager;
            _passwordHasher = passwordHasher;
        }

        public void Add(IdentityUser user)
        {
            IdentityUser identityUser = new IdentityUser();
            identityUser.Email = user.Email;
            identityUser.UserName = user.UserName;
            identityUser.PasswordHash = _passwordHasher.HashPassword(null,user.PasswordHash);
            _context.Users.Add(identityUser);
            _context.SaveChanges();
            
        }

        public void Delete(IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public bool forgotPassword(UserModel user)
        {
            IdentityUser userLogin = _context.Users.SingleOrDefault(x => x.Email.Equals(user.email));
            if (userLogin != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public IdentityUser GetAccount(string Id)
        {
            var account = _context.Users.SingleOrDefault(x => x.Id.Equals(Id));
            return account;
        }

        public IEnumerable<IdentityUser> GetAll()
        {
            var users = _context.Users.ToList();
            return users;
        }

        public bool Login(UserModel user)
        {
            IdentityUser userLogin = _context.Users.SingleOrDefault(x => x.Email.Equals(user.email));

            if (userLogin != null)
            {
                // Sử dụng VerifyHashedPassword để kiểm tra mật khẩu
                PasswordVerificationResult result = _passwordHasher.VerifyHashedPassword(null, userLogin.PasswordHash, user.password);

                if (result == PasswordVerificationResult.Success)
                {
                    return true; // Mật khẩu khớp
                }
                else
                {
                    return false; // Mật khẩu không khớp
                }
            }
            else
            {
                return false; // Người dùng không tồn tại
            }
        }

        public void Update(IdentityUser user)
        {
            IdentityUser identityUser = GetAccount(user.Id);
            if (identityUser != null)
            {
                identityUser.PasswordHash = _passwordHasher.HashPassword(null,user.PasswordHash);
                _context.Users.Update(identityUser);
                _context.SaveChanges();
            }
        }

        public string GenerateToken(UserModel user)
        {
            IdentityUser userLogin = _context.Users.SingleOrDefault(x => x.Email.Equals(user.email));
            var Roleid = _context.UserRoles.SingleOrDefault(x => x.UserId.Equals(userLogin.Id));
            var Role = _context.Roles.SingleOrDefault(x => x.Id.Equals(Roleid.RoleId));
            var userSession = new UserSession(userLogin.Id, userLogin.Email, Role.Name);

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]!));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var userClaims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, userSession.Id), // Sử dụng email của UserModel làm NameIdentifier
                new Claim(ClaimTypes.Email, userSession.Email),
                new Claim(ClaimTypes.Role, userSession.Role)
            };

            // Không thêm vai trò vào danh sách claim vì UserModel không chứa thông tin về vai trò

            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Audience"],
                claims: userClaims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public void AddRole(IdentityUserRole<string> user)
        {
            IdentityUserRole<string> identityUserRole = new IdentityUserRole<string>();
            identityUserRole.UserId = user.UserId;
            identityUserRole.RoleId = user.RoleId;
            _context.UserRoles.Add(identityUserRole);
            _context.SaveChanges();
        }

        public IdentityUser GetAccountByEmail(string Email)
        {
            var account = _context.Users.SingleOrDefault(x => x.Email.Equals(Email));
            return account;
        }
    }
}
