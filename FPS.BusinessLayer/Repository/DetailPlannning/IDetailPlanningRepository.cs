﻿using FPS.BusinessLayer.ViewModel.DetailPlanning;
using FPS.BusinessLayer.ViewModel.DetailPlannning;
using FPS.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.DetailPlannning
{
    public interface IDetailPlanningRepository
    {
        public void ImportExecel(DepartmentPlanningDetailModel model);
        public IEnumerable<DetailPlanningExecel> GetExcel(Guid Id);
    }
}
