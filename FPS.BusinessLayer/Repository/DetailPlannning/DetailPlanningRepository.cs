﻿using FPS.BusinessLayer.ViewModel.DetailPlanning;
using FPS.BusinessLayer.ViewModel.DetailPlannning;
using FPS.DataLayer.Context;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.DetailPlannning
{
    public class DetailPlanningRepository : BaseRepository, IDetailPlanningRepository
    {
        public DetailPlanningRepository(FPSContext context) : base(context)
        {
        }

        public List<DetailPlanningExecel> ReadDetailPlanningFromExcel(string filePath)
        {
            List<DetailPlanningExecel> detailPlannings = new List<DetailPlanningExecel>();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(new FileInfo(filePath)))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[0];

                int startRow = 3;
                int endColumn = 12;

                int rowCount = worksheet.Dimension.Rows;

                int row = startRow;
                while (row <= 5)
                {
                    DetailPlanningExecel detail = new DetailPlanningExecel();

                    detail.DateTime = DateTime.Parse(worksheet.Cells[row, 1].Value?.ToString());
                    detail.Term = worksheet.Cells[row, 2].Value?.ToString();
                    detail.Department = worksheet.Cells[row, 3].Value?.ToString();
                    detail.Expense = worksheet.Cells[row, 4].Value?.ToString();
                    detail.CostType = worksheet.Cells[row, 5].Value?.ToString();
                    detail.UnitPrice = double.Parse(worksheet.Cells[row, 6].Value?.ToString());
                    detail.Amount = int.Parse(worksheet.Cells[row, 7].Value?.ToString());
                    detail.Total = double.Parse(worksheet.Cells[row, 8].Value?.ToString());
                    detail.ProjectName = worksheet.Cells[row, 9].Value?.ToString();
                    detail.SuplierName = worksheet.Cells[row, 10].Value?.ToString();
                    detail.PriceToCost = double.Parse(worksheet.Cells[row, 11].Value?.ToString());
                    detail.Note = worksheet.Cells[row, 12].Value?.ToString();

                    if (string.IsNullOrEmpty(detail.ProjectName))
                    {
                        break; // Dừng vòng lặp nếu không còn dữ liệu
                    }

                    detailPlannings.Add(detail);
                    row++;
                }
            }

            return detailPlannings;
        }

        public IEnumerable<DetailPlanningExecel> GetExcel(Guid Id)
        {
            string filePath = _context.departmentPlanningDetails.FirstOrDefault(x => x.DepartmentPlanID == Id).LinkFile.ToString();
            var excel = ReadDetailPlanningFromExcel(filePath);
            var resultExcel = excel.Select(detail => new DetailPlanningExecel
            {
                Term = detail.Term,
                Amount = detail.Amount,
                CostType = detail.CostType,
                DateTime = detail.DateTime,
                Department = detail.Department,
                Expense = detail.Expense,
                Note = detail.Note,
                Total = detail.Total,
                UnitPrice = detail.UnitPrice,
                SuplierName = detail.SuplierName,
                ProjectName = detail.ProjectName,
                PriceToCost = detail.PriceToCost,

            }).ToList();
            return resultExcel;
        }

        public void ImportExecel(DepartmentPlanningDetailModel model)
        {
            FPS.DataLayer.Entity.DepartmentPlanningDetail departmentPlanningdetail = new DataLayer.Entity.DepartmentPlanningDetail();
            departmentPlanningdetail.LinkFile = model.LinkFile;
            departmentPlanningdetail.DepartmentPlanID = model.DepartmentId;
            departmentPlanningdetail.CostID = model.CostId;
            _context.departmentPlanningDetails.Add(departmentPlanningdetail);
            _context.SaveChanges();
        }
    }
}
