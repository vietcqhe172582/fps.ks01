﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.DataLayer.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Company
{
    public class CompanyRepository : BaseRepository,ICompanyRepository
    {
        public CompanyRepository(FPSContext context) : base(context)
        {
        }

        public void AddCompany(CompanyModel model)
        {
            FPS.DataLayer.Entity.Company company = new FPS.DataLayer.Entity.Company();
            company.Id = Guid.NewGuid();
            company.CompanyName = model.CompanyName;
            company.CreateDate = DateTime.Now;
            company.UpdateDate = DateTime.Now;
            company.CreateBy = model.CreateBy;
            company.UpdatedBy = model.UpdatedBy;
            company.IsDeleted = false;
            _context.Companies.Add(company);
            _context.SaveChanges();
        }

        public void DeleteCompany(Guid id)
        {
            var company = _context.Companies.SingleOrDefault(x => x.Id == id);
            company.IsDeleted = true;
            _context.Companies.Update(company);
            _context.SaveChanges();
        }

        public IEnumerable<CompanyViewModel> GetAll()
        {
            var companies = _context.Companies.ToList();
            var companyModels = companies.Select(company => new CompanyViewModel
            {

                Id = company.Id,
                CompanyName = company.CompanyName,

            }).ToList();

            return companyModels;
        }

        public FPS.DataLayer.Entity.Company GetCompany(Guid id)
        {
            var company = _context.Companies.FirstOrDefault(x => x.Id == id);

            return company;
        }

        public void UpdateCompany(CompanyModel model)
        {
            FPS.DataLayer.Entity.Company company = GetCompany(model.Id);
            company.CompanyName = model.CompanyName;
            company.CreateDate = model.CreateDate;
            company.UpdateDate = model.UpdateDate;
            company.CreateBy = model.CreateBy;
            company.UpdatedBy = model.UpdatedBy;
            company.IsDeleted = model.IsDeleted;
            _context.Companies.Update(company);
            _context.SaveChanges();
        }
    }
}
