﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Company
{
    public interface ICompanyRepository
    {
        public void AddCompany(CompanyModel model);
        public void UpdateCompany(CompanyModel model);
        public void DeleteCompany(Guid Id);
        public IEnumerable<CompanyViewModel> GetAll();
        public FPS.DataLayer.Entity.Company GetCompany(Guid id);
    }
}
