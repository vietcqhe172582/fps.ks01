﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.BusinessLayer.ViewModel.MonthPlanning;
using FPS.DataLayer.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.MonthPlanning
{
    public class MonthPlanningRepository : BaseRepository, IMonthPlanningRepository
    {
        public MonthPlanningRepository(FPSContext context) : base(context)
        {
        }

        public void AddMonthPlanning(MonthPlanningModel model)
        {
            FPS.DataLayer.Entity.MonthPlanning monthPlanning = new FPS.DataLayer.Entity.MonthPlanning();
            monthPlanning.TermName = model.TermName;
            monthPlanning.StartedDate = model.StartedDate;
            monthPlanning.EndDate = model.EndDate;
            monthPlanning.ReportDueDate = model.ReportDueDate;
            monthPlanning.PlanDueDate = model.PlanDueDate;
            monthPlanning.Status = model.Status;
            monthPlanning.Duration = model.Duration;
            monthPlanning.AccountantID = model.AccountantID;
            _context.MonthPlannings.Add(monthPlanning);
            _context.SaveChanges();
        }

        public void DeleteMonthPlanning(Guid Id)
        {
            var monthPlanning = _context.MonthPlannings.SingleOrDefault(x => x.Id == Id);
            monthPlanning.IsDeleted = true;
            _context.MonthPlannings.Update(monthPlanning);
            _context.SaveChanges();
        }

        public IEnumerable<MonthPlanningView> GetAll()
        {
            var monthplannings = _context.MonthPlannings.ToList();
            var monthplanningView = monthplannings.Select(monthplanning => new MonthPlanningView
            {
                ID = monthplanning.Id.ToString(),
                TermName = monthplanning.TermName,
                StartDate = (DateTime)monthplanning.StartedDate,
                EndDate = (DateTime)monthplanning.EndDate,
                ReportDueDate = (DateTime)monthplanning.ReportDueDate,
                PlanDueDate = (DateTime)monthplanning.PlanDueDate,
                Status = monthplanning.Status,
                Duration = monthplanning.Duration

            }).ToList();
            return monthplanningView;
        }

        public DataLayer.Entity.MonthPlanning GetMonthPlanning(Guid id)
        {
            var monthPlanning = _context.MonthPlannings.FirstOrDefault(x => x.Id == id);

            return monthPlanning;
        }

        public void StartedTerm(Guid Id)
        {
            var monthPlanning = _context.MonthPlannings.SingleOrDefault(x => x.Id == Id);
            monthPlanning.Status = "In Process";
            _context.MonthPlannings.Update(monthPlanning);
            _context.SaveChanges();
        }

        public void UpdateMonthPlanning(MonthPlanningModel model)
        {
            FPS.DataLayer.Entity.MonthPlanning monthPlanning = GetMonthPlanning(model.Id);
            monthPlanning.TermName = model.TermName;
            monthPlanning.StartedDate = model.StartedDate;
            monthPlanning.EndDate = model.EndDate;
            monthPlanning.PlanDueDate = model.PlanDueDate;
            monthPlanning.ReportDueDate = model.ReportDueDate;
            _context.MonthPlannings.Update(monthPlanning);
            _context.SaveChanges();
        }
    }
}
