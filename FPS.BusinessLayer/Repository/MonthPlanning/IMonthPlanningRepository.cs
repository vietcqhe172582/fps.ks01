﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.BusinessLayer.ViewModel.MonthPlanning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.MonthPlanning
{
    public interface IMonthPlanningRepository
    {
        public void AddMonthPlanning(MonthPlanningModel model);
        public void UpdateMonthPlanning(MonthPlanningModel model);
        public void DeleteMonthPlanning(Guid Id);
        public IEnumerable<MonthPlanningView> GetAll();
        public FPS.DataLayer.Entity.MonthPlanning GetMonthPlanning(Guid id);
        public void StartedTerm(Guid Id);
    }
}
