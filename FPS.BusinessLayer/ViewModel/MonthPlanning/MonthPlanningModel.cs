﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.ViewModel.MonthPlanning
{
    public class MonthPlanningModel : BaseModel
    {
        public string TermName { get; set; }
        public DateTime? StartedDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? PlanDueDate { get; set; }
        public DateTime? ReportDueDate { get; set; }
        public string Duration { get; set; }
        public string Status { get; set; }
        public Guid AccountantID { get; set; }
    }
}
