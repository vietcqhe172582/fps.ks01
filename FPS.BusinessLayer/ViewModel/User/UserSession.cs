﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.ViewModel.User
{
    public record UserSession(string? Id,string? Email, string? Role);
}
