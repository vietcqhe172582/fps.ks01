﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.ViewModel.User
{
    public class AccountLogin
    {
        public bool IsLoggedIn { get; set; }
        public string Token { get; set; }
    }
}
