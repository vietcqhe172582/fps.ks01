﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.ViewModel.DepartmentPlanning
{
    public class DepartmentPlanningView
    {
        public string ID { get; set; }
        public string DepartmentPlanningName { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
