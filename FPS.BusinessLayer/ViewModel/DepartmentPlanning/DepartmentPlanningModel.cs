﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.ViewModel.DepartmentPlanning
{
    public class DepartmentPlanningModel : BaseModel
    {
        public string DepartmentPlanningName { get; set; }
        public string TermID { get; set; }
        public string FinncialStaffID { get; set; }
    }
}
