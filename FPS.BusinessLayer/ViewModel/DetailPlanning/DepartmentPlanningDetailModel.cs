﻿using FPS.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.ViewModel.DetailPlanning
{
    public class DepartmentPlanningDetailModel : BaseModel
    {
        public string LinkFile { get; set; }
        public Guid DepartmentId { get; set; }
        public Guid CostId { get; set; }
    }
}
