﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.ViewModel.DetailPlannning
{
    public class DetailPlanningExecel
    {
        public DateTime DateTime { get; set; }
        public string Term { get; set; }
        public string Department { get; set; }
        public string Expense { get; set; }
        public string CostType { get; set; }
        public double UnitPrice { get; set; }
        public int Amount { get; set; }
        public double Total { get; set; }
        public string ProjectName { get; set; }
        public string SuplierName { get; set; }
        public double PriceToCost { get; set; }
        public string Note { get; set; }

    }
}
