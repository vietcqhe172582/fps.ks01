﻿namespace FPS.API.Email
{
    public interface IMailService
    {
        bool SendMail(MailData mailData);
    }
}
