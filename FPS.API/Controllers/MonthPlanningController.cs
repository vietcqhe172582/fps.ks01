﻿using FPS.BusinessLayer.Repository.MonthPlanning;
using FPS.BusinessLayer.ViewModel.Company;
using FPS.BusinessLayer.ViewModel.MonthPlanning;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Accountant")]
    [ApiController]
    public class MonthPlanningController : ControllerBase
    {
        private readonly IMonthPlanningRepository _monthPlanningRepository;
        public MonthPlanningController(IMonthPlanningRepository monthPlanningRepository)
        {
            _monthPlanningRepository = monthPlanningRepository;
        }

        [HttpPost]
        [Route("AddMonthPlanning")]
        public void AddMonthPlanning(MonthPlanningModel model)
        {
            _monthPlanningRepository.AddMonthPlanning(model);
        }

        [HttpGet]
        [Route("GetAll")]
        public IEnumerable<MonthPlanningView> GetAllMonthPlanning()
        {
            return _monthPlanningRepository.GetAll();
        }

        [HttpGet]
        [Route("GetMonthPlanningByID")]

        public FPS.DataLayer.Entity.MonthPlanning GetMonthPlanningByID(Guid Id)
        {
            return _monthPlanningRepository.GetMonthPlanning(Id);
        }

        [HttpDelete]
        [Route("DeleteMonthPlanning")]
        public void DeleteMonthPlanning(Guid Id)
        {
            _monthPlanningRepository.DeleteMonthPlanning(Id);
        }

        [HttpPut]
        [Route("StartMonthPlanning")]
        public void StartMonthPlanning(Guid Id)
        {
            _monthPlanningRepository.StartedTerm(Id);
        }


        [HttpPut]
        [Route("UpdateMonthPlanning")]
        public void UpdateMonthPlanning(MonthPlanningModel model)
        {
            _monthPlanningRepository.UpdateMonthPlanning(model);
        }
    }
}
