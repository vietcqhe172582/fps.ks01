﻿using FPS.BusinessLayer.Repository.Company;
using FPS.BusinessLayer.ViewModel.Company;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Accountant")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        public readonly ICompanyRepository _companyRepository;

        public CompanyController(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        [HttpDelete]
        [Route("DeleteCompany")]
        public void DeleteCompany(Guid Id)
        {
            _companyRepository.DeleteCompany(Id);
        }

        [HttpPost]
        [Route("AddCompany")]
        public void AddCompany(CompanyModel model)
        {
            _companyRepository.AddCompany(model);
        }

        [HttpPut]
        [Route("UpdateCompany")]
        public void UpdateCompany(CompanyModel model)
        {
            _companyRepository.UpdateCompany(model);
        }

        [HttpGet]
        [Route("GetAll")]
        public IEnumerable<CompanyViewModel> GetCompany()
        {
            return _companyRepository.GetAll();
        }

        [HttpGet]
        [Route("GetCompanyByID")]

        public FPS.DataLayer.Entity.Company GetCompanyByID(Guid Id)
        {
            return _companyRepository.GetCompany(Id);
        }
    }
}
