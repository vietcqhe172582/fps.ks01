﻿using FPS.BusinessLayer.Repository.DepartmentPlannning;
using FPS.BusinessLayer.ViewModel.Company;
using FPS.BusinessLayer.ViewModel.DepartmentPlanning;
using FPS.DataLayer.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentPlanningController : ControllerBase
    {
        private readonly IDepartmentPlanningRepository _departmentPlanningRepository;
        public DepartmentPlanningController(IDepartmentPlanningRepository departmentPlanningRepository)
        {
            _departmentPlanningRepository = departmentPlanningRepository;
        }

        [HttpPost]
        [Route("AddDepartmentPlanning")]
        public void AddDepartmentPlanning(DepartmentPlanningModel model)
        {
            _departmentPlanningRepository.AddDepartmentPlanning(model);
        }

        [HttpGet]
        [Route("GetAllDepartmentPlanning")]
        public IEnumerable<DepartmentPlanningView> GetDepartmentPlanning()
        {
            return _departmentPlanningRepository.GetAll();
        }

        [HttpGet]
        [Route("GetAllDepartmentPlanningByTerm")]
        public IEnumerable<DepartmentPlanningView> GetDepartmentPlanningByTerm(Guid TermID)
        {
            return _departmentPlanningRepository.GetPlanningByTermID(TermID);
        }

        [HttpGet]
        [Route("GetDepartmentPlanningByID")]

        public FPS.DataLayer.Entity.DepartmentPlanning GetDepartmentPlanningID(Guid Id)
        {
            return _departmentPlanningRepository.GetDepartmentPlanning(Id);
        }
    }
}
