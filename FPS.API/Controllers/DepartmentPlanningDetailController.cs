﻿using FPS.BusinessLayer.Repository.DetailPlannning;
using FPS.BusinessLayer.ViewModel.Company;
using FPS.BusinessLayer.ViewModel.DetailPlanning;
using FPS.BusinessLayer.ViewModel.DetailPlannning;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentPlanningDetailController : ControllerBase
    {
        public readonly IDetailPlanningRepository _detailPlanningRepository;
        public DepartmentPlanningDetailController(IDetailPlanningRepository detailPlanningRepository)
        {
            _detailPlanningRepository = detailPlanningRepository;
        }

        [HttpGet]
        [Route("GetExcel")]

        public IEnumerable<DetailPlanningExecel> GetExcel(Guid Id)
        {
            return _detailPlanningRepository.GetExcel(Id);
        }

        [HttpPost]
        [Route("AddPlanning")]
        public void AddPlanning(DepartmentPlanningDetailModel model)
        {
            _detailPlanningRepository.ImportExecel(model);
        }
    }
}
