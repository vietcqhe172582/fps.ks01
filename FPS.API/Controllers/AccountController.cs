﻿using FPS.BusinessLayer.Repository.Company;
using FPS.BusinessLayer.Repository.User;
using FPS.BusinessLayer.ViewModel.Company;
using FPS.BusinessLayer.ViewModel.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        public readonly IAccountRepository _accountRepository;
        public AccountController(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        [HttpGet]
        [Route("GetAccount")]

        public IActionResult Get(string id)
        {
            IdentityUser data = _accountRepository.GetAccount(id); // Dữ liệu mẫu
            return Ok(data); // Trả về dữ liệu dưới dạng JSON
        }

        [HttpGet]
        [Route("GetAccountByEmail")]

        public IActionResult GetByEmail(string email)
        {
            IdentityUser data = _accountRepository.GetAccountByEmail(email); // Dữ liệu mẫu
            return Ok(data); // Trả về dữ liệu dưới dạng JSON
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("GetAll")]
        public IActionResult GetAll()
        {
            var data = _accountRepository.GetAll(); // Dữ liệu mẫu
            return Ok(data); // Trả về dữ liệu dưới dạng JSON
        }

        [HttpPost]
        [Route("AddAccount")]
        public void AddAccount(IdentityUser user)
        {
            _accountRepository.Add(user);
        }

        [HttpPost]
        [Route("AddUserRole")]
        public void AddUserRole(IdentityUserRole<string> user)
        {
            _accountRepository.AddRole(user);
        }

        [HttpPost]
        [Route("Login")]
        public IActionResult Login(UserModel user)
        {
            if (_accountRepository.Login(user))
            {
                // Đăng nhập thành công, tạo và trả về token
                string token = _accountRepository.GenerateToken(user);
                return Ok(new { Token = token });
            }
            else
            {
                return BadRequest("Invalid email or password");
            }
        }

        [HttpPut]
        [Route("UpdateUser")]
        public void UpdateUser(IdentityUser model)
        {
            _accountRepository.Update(model);
        }

        [HttpPost]
        [Route("ForgotPassword")]
        public IActionResult forgotPassword(UserModel user)
        {
            if (_accountRepository.forgotPassword(user))
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
