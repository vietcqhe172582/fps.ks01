﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Entity
{
	public class EntityBase
	{
		public Guid Id { get; set; }
		public Guid? CreateBy { get; set; }
		public DateTime? CreateDate { get; set; }
		public Guid UpdatedBy { get; set; }
		public DateTime? UpdateDate { get; set; }
		public bool IsDeleted { get; set; }
	}
}
