﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Entity
{
    public class DepartmentPlanning : EntityBase
    {
        public string DepartmentPlanningName { get; set; }
        public Guid TermID { get; set; }
        public Guid FinancialStaffID { get; set; }
        public MonthPlanning MonthPlanning { get; set; }
        public DepartmentPlanningDetail DepartmentPlanningDetail { get; set;}
    }
}
