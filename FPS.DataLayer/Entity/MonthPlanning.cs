﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Entity
{
    public class MonthPlanning : EntityBase
    {
        public string TermName { get; set; }
        public DateTime? StartedDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? PlanDueDate { get; set;}
        public DateTime? ReportDueDate { get; set; }
        public string Duration { get; set; }
        public string Status { get; set; }
        public Guid AccountantID { get; set; }

        public ICollection<DepartmentPlanning> departmentPlannings { get; set; }
    }
}
