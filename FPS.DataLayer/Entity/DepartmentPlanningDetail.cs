﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Entity
{
    public class DepartmentPlanningDetail : EntityBase
    {
        public string LinkFile { get; set; }
        public Guid DepartmentPlanID { get; set; }
        public Guid CostID { get; set; }
        public DepartmentPlanning DepartmentPlanning { get; set; }
        public Cost Cost { get; set; }
    }
}
