﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Entity
{
    public class Cost : EntityBase
    {
        public string CostName { get; set; }

        public DepartmentPlanningDetail DepartmentPlanningDetail { get; set;}
    }
}
