﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FPS.DataLayer.Migrations
{
    /// <inheritdoc />
    public partial class updatenewDb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Role_Company_CompanyId",
                table: "Role");

            migrationBuilder.DropIndex(
                name: "IX_Role_CompanyId",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "Role");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "CompanyId",
                table: "Role",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Role_CompanyId",
                table: "Role",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Role_Company_CompanyId",
                table: "Role",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id");
        }
    }
}
