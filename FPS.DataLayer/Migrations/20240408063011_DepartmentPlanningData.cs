﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace FPS.DataLayer.Migrations
{
    /// <inheritdoc />
    public partial class DepartmentPlanningData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "9f8fdadc-da08-401a-8865-66c6c03c517b");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a421e06e-778e-4892-8c72-53c4fb5236bc");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "bc014cd3-6afc-4008-a785-f6f7183bc9fd");

            migrationBuilder.DeleteData(
                table: "Cost",
                keyColumn: "Id",
                keyValue: new Guid("000761c1-f451-433a-98de-e2bdf764d2c4"));

            migrationBuilder.DeleteData(
                table: "Cost",
                keyColumn: "Id",
                keyValue: new Guid("7c8db523-7f15-4645-ac70-f516920473e4"));

            migrationBuilder.DeleteData(
                table: "Cost",
                keyColumn: "Id",
                keyValue: new Guid("a94edde8-aeca-4356-89c1-18996f4365d1"));

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "233c2b23-7fd7-4ad1-bade-3bd2d9252cf2", null, "Accountant", null },
                    { "4a825fae-c462-4978-967f-2774991732f1", null, "Admin", null },
                    { "54ebbc9a-ddbf-4df6-a0f7-08d4a6395a3d", null, "Finncial Staff", null }
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "11111111",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "145f28bf-8ca7-40c6-8aac-936873efb9bf", "660d7c36-6edb-4443-bf8a-a484315a83e8" });

            migrationBuilder.InsertData(
                table: "Cost",
                columns: new[] { "Id", "CostName", "CreateBy", "CreateDate", "IsDeleted", "UpdateDate", "UpdatedBy" },
                values: new object[,]
                {
                    { new Guid("83a8988d-6521-4d08-bc96-13db387191af"), "US", new Guid("b0e8e58d-4f0d-4907-aee9-5d7dda255cbc"), new DateTime(2024, 4, 8, 13, 30, 11, 310, DateTimeKind.Local).AddTicks(3207), false, new DateTime(2024, 4, 8, 13, 30, 11, 310, DateTimeKind.Local).AddTicks(3208), new Guid("7098344d-0302-4bb7-9375-2f7bd243eee1") },
                    { new Guid("939aee5b-b613-4589-9c70-7bf7f02d4286"), "Euro", new Guid("ddc72cfe-57bc-4904-8ab7-2d988818493d"), new DateTime(2024, 4, 8, 13, 30, 11, 310, DateTimeKind.Local).AddTicks(3201), false, new DateTime(2024, 4, 8, 13, 30, 11, 310, DateTimeKind.Local).AddTicks(3202), new Guid("c5e54ab5-1def-4e29-b08c-586ee3e59a8d") },
                    { new Guid("fbebf34f-6587-4624-97bf-e2be75e15e19"), "VietNameDong", new Guid("ebb1bc53-c22c-44c2-80f6-57d4c67106e4"), new DateTime(2024, 4, 8, 13, 30, 11, 310, DateTimeKind.Local).AddTicks(3197), false, new DateTime(2024, 4, 8, 13, 30, 11, 310, DateTimeKind.Local).AddTicks(3199), new Guid("9e8312ae-35ef-49a9-85a9-a664d0ac7e3d") }
                });

            migrationBuilder.InsertData(
                table: "DepartmentPlanning",
                columns: new[] { "Id", "CreateBy", "CreateDate", "DepartmentPlanningName", "FinancialStaffID", "IsDeleted", "TermID", "UpdateDate", "UpdatedBy" },
                values: new object[,]
                {
                    { new Guid("489d293e-b7e7-4d8d-a352-c2702086df9d"), new Guid("63ebcaeb-932c-4e26-96ab-fdca557726b2"), new DateTime(2024, 4, 8, 13, 30, 11, 310, DateTimeKind.Local).AddTicks(2276), "Department 2", new Guid("cdb9c80a-49aa-46a9-acc1-a254bd7ba7e5"), false, new Guid("47530741-5ef9-48ee-d7db-08dc55772585"), new DateTime(2024, 4, 8, 13, 30, 11, 310, DateTimeKind.Local).AddTicks(2277), new Guid("2b8c5c3a-1408-4863-8def-5d899d7e32a2") },
                    { new Guid("8f095fdb-c74b-4692-8533-e4e9f0506b39"), new Guid("82f27c22-555d-48ae-a8fd-5e50e49cd8a8"), new DateTime(2024, 4, 8, 13, 30, 11, 310, DateTimeKind.Local).AddTicks(2282), "Department 3", new Guid("3f52e8c3-301a-4f40-8850-3257d4fc5a4a"), false, new Guid("47530741-5ef9-48ee-d7db-08dc55772585"), new DateTime(2024, 4, 8, 13, 30, 11, 310, DateTimeKind.Local).AddTicks(2283), new Guid("dd69d6e0-b69b-4199-80ec-e6b2e2971a1d") },
                    { new Guid("f67a2b32-51df-422b-b661-61adc8d0286b"), new Guid("fbf73e18-0d62-4bec-9e88-4651c06b909b"), new DateTime(2024, 4, 8, 13, 30, 11, 310, DateTimeKind.Local).AddTicks(2258), "Department 1", new Guid("638e5d78-0ecb-4137-92ef-f9e978072dd5"), false, new Guid("47530741-5ef9-48ee-d7db-08dc55772585"), new DateTime(2024, 4, 8, 13, 30, 11, 310, DateTimeKind.Local).AddTicks(2272), new Guid("cce4e15b-82a3-4645-8a7e-58d78e9fb592") }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "233c2b23-7fd7-4ad1-bade-3bd2d9252cf2");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "4a825fae-c462-4978-967f-2774991732f1");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "54ebbc9a-ddbf-4df6-a0f7-08d4a6395a3d");

            migrationBuilder.DeleteData(
                table: "Cost",
                keyColumn: "Id",
                keyValue: new Guid("83a8988d-6521-4d08-bc96-13db387191af"));

            migrationBuilder.DeleteData(
                table: "Cost",
                keyColumn: "Id",
                keyValue: new Guid("939aee5b-b613-4589-9c70-7bf7f02d4286"));

            migrationBuilder.DeleteData(
                table: "Cost",
                keyColumn: "Id",
                keyValue: new Guid("fbebf34f-6587-4624-97bf-e2be75e15e19"));

            migrationBuilder.DeleteData(
                table: "DepartmentPlanning",
                keyColumn: "Id",
                keyValue: new Guid("489d293e-b7e7-4d8d-a352-c2702086df9d"));

            migrationBuilder.DeleteData(
                table: "DepartmentPlanning",
                keyColumn: "Id",
                keyValue: new Guid("8f095fdb-c74b-4692-8533-e4e9f0506b39"));

            migrationBuilder.DeleteData(
                table: "DepartmentPlanning",
                keyColumn: "Id",
                keyValue: new Guid("f67a2b32-51df-422b-b661-61adc8d0286b"));

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "9f8fdadc-da08-401a-8865-66c6c03c517b", null, "Accountant", null },
                    { "a421e06e-778e-4892-8c72-53c4fb5236bc", null, "Admin", null },
                    { "bc014cd3-6afc-4008-a785-f6f7183bc9fd", null, "Finncial Staff", null }
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "11111111",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "66c3b20c-7f90-4a8c-a88e-c3a1745999a2", "9ec8edb9-b7f1-41d1-8b63-c8b7f16c98ad" });

            migrationBuilder.InsertData(
                table: "Cost",
                columns: new[] { "Id", "CostName", "CreateBy", "CreateDate", "IsDeleted", "UpdateDate", "UpdatedBy" },
                values: new object[,]
                {
                    { new Guid("000761c1-f451-433a-98de-e2bdf764d2c4"), "Euro", new Guid("9549597c-cbd2-4aec-9333-8749374e41a9"), new DateTime(2024, 4, 8, 13, 25, 19, 434, DateTimeKind.Local).AddTicks(7745), false, new DateTime(2024, 4, 8, 13, 25, 19, 434, DateTimeKind.Local).AddTicks(7746), new Guid("2a154735-baa2-46e8-acfe-455b687ec0cd") },
                    { new Guid("7c8db523-7f15-4645-ac70-f516920473e4"), "US", new Guid("aa36ea82-14a7-4548-8c00-3404b257e44b"), new DateTime(2024, 4, 8, 13, 25, 19, 434, DateTimeKind.Local).AddTicks(7748), false, new DateTime(2024, 4, 8, 13, 25, 19, 434, DateTimeKind.Local).AddTicks(7749), new Guid("6cd141cd-2787-46d4-9a8e-044eef5a3b28") },
                    { new Guid("a94edde8-aeca-4356-89c1-18996f4365d1"), "VietNameDong", new Guid("7d41873b-6254-4763-8afd-ee24f76d9bdf"), new DateTime(2024, 4, 8, 13, 25, 19, 434, DateTimeKind.Local).AddTicks(7717), false, new DateTime(2024, 4, 8, 13, 25, 19, 434, DateTimeKind.Local).AddTicks(7732), new Guid("d25395de-21ec-49f9-a4cc-787332bdb74b") }
                });
        }
    }
}
