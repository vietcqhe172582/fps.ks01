﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FPS.DataLayer.Migrations
{
    /// <inheritdoc />
    public partial class adddata : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "11111111", 0, "82262b2d-3887-4045-bd36-e745f093f2cd", "user1@example.com", true, true, null, "USER1@EXAMPLE.COM", "USER1", null, "123456789", true, "688ed4ec-548d-4012-811f-c29d93a118f1", false, "user1" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "11111111");
        }
    }
}
