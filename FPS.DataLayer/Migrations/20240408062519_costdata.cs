﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace FPS.DataLayer.Migrations
{
    /// <inheritdoc />
    public partial class costdata : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "6418b6de-a281-4485-a93b-c0314e6af3e0");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b956fc20-4525-4923-a579-652af750bef1");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "edc0c0c6-b64e-41fe-9c22-5858a1196d3f");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "9f8fdadc-da08-401a-8865-66c6c03c517b", null, "Accountant", null },
                    { "a421e06e-778e-4892-8c72-53c4fb5236bc", null, "Admin", null },
                    { "bc014cd3-6afc-4008-a785-f6f7183bc9fd", null, "Finncial Staff", null }
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "11111111",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "66c3b20c-7f90-4a8c-a88e-c3a1745999a2", "9ec8edb9-b7f1-41d1-8b63-c8b7f16c98ad" });

            migrationBuilder.InsertData(
                table: "Cost",
                columns: new[] { "Id", "CostName", "CreateBy", "CreateDate", "IsDeleted", "UpdateDate", "UpdatedBy" },
                values: new object[,]
                {
                    { new Guid("000761c1-f451-433a-98de-e2bdf764d2c4"), "Euro", new Guid("9549597c-cbd2-4aec-9333-8749374e41a9"), new DateTime(2024, 4, 8, 13, 25, 19, 434, DateTimeKind.Local).AddTicks(7745), false, new DateTime(2024, 4, 8, 13, 25, 19, 434, DateTimeKind.Local).AddTicks(7746), new Guid("2a154735-baa2-46e8-acfe-455b687ec0cd") },
                    { new Guid("7c8db523-7f15-4645-ac70-f516920473e4"), "US", new Guid("aa36ea82-14a7-4548-8c00-3404b257e44b"), new DateTime(2024, 4, 8, 13, 25, 19, 434, DateTimeKind.Local).AddTicks(7748), false, new DateTime(2024, 4, 8, 13, 25, 19, 434, DateTimeKind.Local).AddTicks(7749), new Guid("6cd141cd-2787-46d4-9a8e-044eef5a3b28") },
                    { new Guid("a94edde8-aeca-4356-89c1-18996f4365d1"), "VietNameDong", new Guid("7d41873b-6254-4763-8afd-ee24f76d9bdf"), new DateTime(2024, 4, 8, 13, 25, 19, 434, DateTimeKind.Local).AddTicks(7717), false, new DateTime(2024, 4, 8, 13, 25, 19, 434, DateTimeKind.Local).AddTicks(7732), new Guid("d25395de-21ec-49f9-a4cc-787332bdb74b") }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "9f8fdadc-da08-401a-8865-66c6c03c517b");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a421e06e-778e-4892-8c72-53c4fb5236bc");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "bc014cd3-6afc-4008-a785-f6f7183bc9fd");

            migrationBuilder.DeleteData(
                table: "Cost",
                keyColumn: "Id",
                keyValue: new Guid("000761c1-f451-433a-98de-e2bdf764d2c4"));

            migrationBuilder.DeleteData(
                table: "Cost",
                keyColumn: "Id",
                keyValue: new Guid("7c8db523-7f15-4645-ac70-f516920473e4"));

            migrationBuilder.DeleteData(
                table: "Cost",
                keyColumn: "Id",
                keyValue: new Guid("a94edde8-aeca-4356-89c1-18996f4365d1"));

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "6418b6de-a281-4485-a93b-c0314e6af3e0", null, "Admin", null },
                    { "b956fc20-4525-4923-a579-652af750bef1", null, "Accountant", null },
                    { "edc0c0c6-b64e-41fe-9c22-5858a1196d3f", null, "Finncial Staff", null }
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "11111111",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "e36b72f5-e1c8-49f9-9f79-5aeca00583d7", "0104480e-302d-4655-b7a1-1cebd96f88e2" });
        }
    }
}
