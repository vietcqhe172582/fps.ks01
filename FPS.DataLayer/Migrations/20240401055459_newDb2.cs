﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FPS.DataLayer.Migrations
{
    /// <inheritdoc />
    public partial class newDb2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cost_DepartmentPlanningDetail_Id",
                table: "Cost");

            migrationBuilder.DropForeignKey(
                name: "FK_DepartmentPlanning_DepartmentPlanningDetail_Id",
                table: "DepartmentPlanning");

            migrationBuilder.CreateIndex(
                name: "IX_DepartmentPlanningDetail_CostID",
                table: "DepartmentPlanningDetail",
                column: "CostID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DepartmentPlanningDetail_DepartmentPlanID",
                table: "DepartmentPlanningDetail",
                column: "DepartmentPlanID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_DepartmentPlanningDetail_Cost_CostID",
                table: "DepartmentPlanningDetail",
                column: "CostID",
                principalTable: "Cost",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DepartmentPlanningDetail_DepartmentPlanning_DepartmentPlanID",
                table: "DepartmentPlanningDetail",
                column: "DepartmentPlanID",
                principalTable: "DepartmentPlanning",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DepartmentPlanningDetail_Cost_CostID",
                table: "DepartmentPlanningDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_DepartmentPlanningDetail_DepartmentPlanning_DepartmentPlanID",
                table: "DepartmentPlanningDetail");

            migrationBuilder.DropIndex(
                name: "IX_DepartmentPlanningDetail_CostID",
                table: "DepartmentPlanningDetail");

            migrationBuilder.DropIndex(
                name: "IX_DepartmentPlanningDetail_DepartmentPlanID",
                table: "DepartmentPlanningDetail");

            migrationBuilder.AddForeignKey(
                name: "FK_Cost_DepartmentPlanningDetail_Id",
                table: "Cost",
                column: "Id",
                principalTable: "DepartmentPlanningDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DepartmentPlanning_DepartmentPlanningDetail_Id",
                table: "DepartmentPlanning",
                column: "Id",
                principalTable: "DepartmentPlanningDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
