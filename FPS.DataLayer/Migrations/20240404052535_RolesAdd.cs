﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace FPS.DataLayer.Migrations
{
    /// <inheritdoc />
    public partial class RolesAdd : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "6418b6de-a281-4485-a93b-c0314e6af3e0", null, "Admin", null },
                    { "b956fc20-4525-4923-a579-652af750bef1", null, "Accountant", null },
                    { "edc0c0c6-b64e-41fe-9c22-5858a1196d3f", null, "Finncial Staff", null }
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "11111111",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "e36b72f5-e1c8-49f9-9f79-5aeca00583d7", "0104480e-302d-4655-b7a1-1cebd96f88e2" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "6418b6de-a281-4485-a93b-c0314e6af3e0");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b956fc20-4525-4923-a579-652af750bef1");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "edc0c0c6-b64e-41fe-9c22-5858a1196d3f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "11111111",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "82262b2d-3887-4045-bd36-e745f093f2cd", "688ed4ec-548d-4012-811f-c29d93a118f1" });
        }
    }
}
