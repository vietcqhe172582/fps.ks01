﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FPS.DataLayer.Migrations
{
    /// <inheritdoc />
    public partial class updateDb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "CompanyId",
                table: "Role",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "Role",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "Company",
                type: "datetime2",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MonthPlanning",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TermName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    StartedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PlanDueDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ReportDueDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Duration = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Status = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    AccountantID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreateBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MonthPlanning", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DepartmentPlanning",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DepartmentPlanningName = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    TermID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FinancialStaffID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreateBy = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentPlanning", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DepartmentPlanning_MonthPlanning_TermID",
                        column: x => x.TermID,
                        principalTable: "MonthPlanning",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Role_CompanyId",
                table: "Role",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_DepartmentPlanning_TermID",
                table: "DepartmentPlanning",
                column: "TermID");

            migrationBuilder.AddForeignKey(
                name: "FK_Role_Company_CompanyId",
                table: "Role",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Role_Company_CompanyId",
                table: "Role");

            migrationBuilder.DropTable(
                name: "DepartmentPlanning");

            migrationBuilder.DropTable(
                name: "MonthPlanning");

            migrationBuilder.DropIndex(
                name: "IX_Role_CompanyId",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "Company");
        }
    }
}
