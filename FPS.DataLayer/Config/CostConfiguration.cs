﻿using FPS.DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Config
{
    public class CostConfiguration : IEntityTypeConfiguration<Cost>
    {
        public void Configure(EntityTypeBuilder<Cost> builder)
        {
            builder.ToTable(nameof(Cost));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.CostName).HasMaxLength(100);

            builder.HasData(
                new Cost
                {
                    Id = Guid.NewGuid(),
                    CostName = "VietNameDong",
                    CreateBy = Guid.NewGuid(),
                    CreateDate = DateTime.Now,
                    UpdatedBy = Guid.NewGuid(),
                    UpdateDate = DateTime.Now,
                    IsDeleted = false
                },
                new Cost
                {
                    Id = Guid.NewGuid(),
                    CostName = "Euro",
                    CreateBy = Guid.NewGuid(),
                    CreateDate = DateTime.Now,
                    UpdatedBy = Guid.NewGuid(),
                    UpdateDate = DateTime.Now,
                    IsDeleted = false
                },
                new Cost
                {
                    Id = Guid.NewGuid(),
                    CostName = "US",
                    CreateBy = Guid.NewGuid(),
                    CreateDate = DateTime.Now,
                    UpdatedBy = Guid.NewGuid(),
                    UpdateDate = DateTime.Now,
                    IsDeleted = false
                }
                );
        }
    }
}
