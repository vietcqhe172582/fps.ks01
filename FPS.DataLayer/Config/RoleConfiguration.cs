﻿using FPS.DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Config
{
	public class RoleConfiguration : IEntityTypeConfiguration<Role>
	{
		public void Configure(EntityTypeBuilder<Role> builder)
		{
			builder.ToTable(nameof(Role));
			builder.HasKey(x => x.Id);
			builder.Property(x => x.RoleName).HasMaxLength(50);
			builder.Property(x => x.IsDeleted).HasDefaultValue(false);

		}
	}
}
