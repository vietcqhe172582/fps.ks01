﻿using FPS.DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Config
{
    public class MonthPlanningConfiguration : IEntityTypeConfiguration<MonthPlanning>
    {
        public void Configure(EntityTypeBuilder<MonthPlanning> builder)
        {
            builder.ToTable(nameof(MonthPlanning));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.TermName).HasMaxLength(100);
            builder.Property(x => x.Status).HasMaxLength(50);
        }
    }
}
