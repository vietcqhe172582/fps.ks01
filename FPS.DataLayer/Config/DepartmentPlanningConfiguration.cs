﻿using FPS.DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Config
{
    public class DepartmentPlanningConfiguration : IEntityTypeConfiguration<DepartmentPlanning>
    {
        public void Configure(EntityTypeBuilder<DepartmentPlanning> builder)
        {
            builder.ToTable(nameof(DepartmentPlanning));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.DepartmentPlanningName).HasMaxLength(150);

            builder.HasData(
                new DepartmentPlanning
                {
                    Id = Guid.NewGuid(),
                    DepartmentPlanningName = "Department 1",
                    TermID = Guid.Parse("47530741-5EF9-48EE-D7DB-08DC55772585"),
                    FinancialStaffID = Guid.NewGuid(),
                    CreateBy = Guid.NewGuid(),
                    CreateDate = DateTime.Now,
                    UpdatedBy = Guid.NewGuid(),
                    UpdateDate = DateTime.Now,
                    IsDeleted = false
                },
                new DepartmentPlanning
                {
                    Id = Guid.NewGuid(),
                    DepartmentPlanningName = "Department 2",
                    TermID = Guid.Parse("47530741-5EF9-48EE-D7DB-08DC55772585"),
                    FinancialStaffID = Guid.NewGuid(),
                    CreateBy = Guid.NewGuid(),
                    CreateDate = DateTime.Now,
                    UpdatedBy = Guid.NewGuid(),
                    UpdateDate = DateTime.Now,
                    IsDeleted = false
                },
                new DepartmentPlanning
                {
                    Id = Guid.NewGuid(),
                    DepartmentPlanningName = "Department 3",
                    TermID = Guid.Parse("47530741-5EF9-48EE-D7DB-08DC55772585"),
                    FinancialStaffID = Guid.NewGuid(),
                    CreateBy = Guid.NewGuid(),
                    CreateDate = DateTime.Now,
                    UpdatedBy = Guid.NewGuid(),
                    UpdateDate = DateTime.Now,
                    IsDeleted = false
                }
                );
        }
    }
}
