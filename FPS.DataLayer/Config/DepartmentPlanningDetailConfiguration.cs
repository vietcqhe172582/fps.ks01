﻿using FPS.DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Config
{
    public class DepartmentPlanningDetailConfiguration : IEntityTypeConfiguration<DepartmentPlanningDetail>
    {
        public void Configure(EntityTypeBuilder<DepartmentPlanningDetail> builder)
        {
            builder.ToTable(nameof(DepartmentPlanningDetail));
            builder.HasKey(x => x.Id);
            builder.Property(x => x.LinkFile).HasMaxLength(1000);
        }
    }
}
