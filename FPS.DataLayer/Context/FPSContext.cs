﻿using FPS.DataLayer.Config;
using FPS.DataLayer.Entity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Context
{
    public class FPSContext : IdentityDbContext<IdentityUser>
    {
        public FPSContext(DbContextOptions options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CompanyConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new MonthPlanningConfiguration());
            modelBuilder.ApplyConfiguration(new DepartmentPlanningConfiguration());
            modelBuilder.ApplyConfiguration(new DepartmentPlanningDetailConfiguration());
            modelBuilder.ApplyConfiguration(new CostConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new RolesConfiguration());
        
            modelBuilder.Entity<DepartmentPlanning>()
                        .HasOne(x => x.MonthPlanning)
                        .WithMany(y => y.departmentPlannings)
                        .HasForeignKey(z => z.TermID);
            modelBuilder.Entity<Cost>().HasOne(x => x.DepartmentPlanningDetail).WithOne(y => y.Cost).HasForeignKey<DepartmentPlanningDetail>(z => z.CostID);
            modelBuilder.Entity<DepartmentPlanning>().HasOne(x => x.DepartmentPlanningDetail).WithOne(y => y.DepartmentPlanning).HasForeignKey<DepartmentPlanningDetail>(z => z.DepartmentPlanID);
            base.OnModelCreating(modelBuilder);

            // Configure the primary key for IdentityUserLogin
            modelBuilder.Entity<IdentityUserLogin<string>>().HasKey(l => new { l.LoginProvider, l.ProviderKey });
            modelBuilder.Entity<IdentityUserRole<string>>().HasKey(l => new { l.UserId, l.RoleId });
            modelBuilder.Entity<IdentityUserToken<string>>().HasKey(x => new { x.UserId, x.LoginProvider });
        }

        public DbSet<Company> Companies { get; set; }
        public DbSet<MonthPlanning> MonthPlannings { get; set; }
        public DbSet<DepartmentPlanning> DepartmentPlannings { get; set; }
        public DbSet<DepartmentPlanningDetail> departmentPlanningDetails { get; set; }
        public DbSet<Cost> costs { get; set; }
    }
}
